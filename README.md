For non-FRC applications, see [gstems-common](https://gitlab.com/growingstems).
# growingSTEMS FRC Common Library
main: [![pipeline status](https://gitlab.com/growingstems/frc-836-the-robobees/gstems-frc-common/badges/main/pipeline.svg)](https://gitlab.com/growingstems/frc-836-the-robobees/gstems-frc-common/-/commits/main)<br>
[JavaDoc Here!](https://growingstems.gitlab.io/frc-836-the-robobees/gstems-frc-common/)<br><br>
Java library with general purpose library funtionality as well as FRC specific command based library functionality.<br>
Uses [Spotless](https://github.com/diffplug/spotless) for code formatting. See the [Spotless Gradle](vscode:extension/richardwillis.vscode-spotless-gradle) extension for auto-formatting in VS Code!<br>
For usage, see [this wiki page on GitLab](https://gitlab.com/growingstems/frc-836-the-robobees/gstems-frc-common/-/wikis/Normal%20Usage)
