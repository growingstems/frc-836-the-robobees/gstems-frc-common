/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.frc.test;

import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.Subsystem;

/**
 * Allows instantiation of InstantCommands that can run when the robot is disabled. This allows the
 * CommandScheduler to run these Commands in test cases.
 */
public class TestInstantCommand extends InstantCommand {

    /**
     * Constructor
     *
     * @param toRun the Runnable to run
     * @param requirements the subsystems required by this command
     */
    public TestInstantCommand(Runnable toRun, Subsystem... requirements) {
        super(toRun, requirements);
    }

    /**
     * Override of Command's implementation of runsWhenDisabled. This override enables this command
     * when the robot is disabled
     *
     * @return true
     */
    @Override
    public boolean runsWhenDisabled() {
        return true;
    }
}
