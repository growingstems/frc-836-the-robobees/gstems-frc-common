/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.frc.test;

import edu.wpi.first.wpilibj2.command.CommandScheduler;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

/**
 * Test fixture for functionality that requires the CommandScheduler to be active. Enables the
 * scheduler before each test. Disables, clears, and cancels the scheduler after each test
 */
public class CommandScheduleTest {

    /** Called before each test. Enables the CommandScheduler */
    @BeforeEach
    public void setUp() {
        CommandScheduler.getInstance().enable();
    }

    /**
     * Called after each test. Disables the CommandScheduler and cleans up any leftover Buttons and
     * Tasks
     */
    @AfterEach
    public void tearDown() {
        CommandScheduler.getInstance().disable();
        CommandScheduler.getInstance().getActiveButtonLoop().clear();
        CommandScheduler.getInstance().cancelAll();
    }

    /**
     * Runs a specified number of iterations of the CommandScheduler
     *
     * @param frames Number of iterations to run
     */
    public void step(int frames) {
        for (int i = 0; i < frames; i++) {
            CommandScheduler.getInstance().run();
        }
    }

    /** Runs one iteration of the CommandScheduler */
    public void step() {
        step(1);
    }
}
