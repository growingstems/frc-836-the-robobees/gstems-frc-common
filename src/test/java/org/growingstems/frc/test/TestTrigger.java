/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.frc.test;

import edu.wpi.first.wpilibj2.command.Subsystem;
import edu.wpi.first.wpilibj2.command.button.Trigger;
import java.util.function.BooleanSupplier;

/**
 * Allows instantiation of Triggers that can run when the robot is disabled. This allows the
 * CommandScheduler to run the associated Commands in test cases.
 */
public class TestTrigger extends Trigger {

    /**
     * Constructor
     *
     * @param isActive returns whether or not the trigger should be active
     */
    public TestTrigger(BooleanSupplier isActive) {
        super(isActive);
    }

    /**
     * Starts the given command whenever the trigger just becomes active. Uses TestInstantCommand to
     * allow the runnable to be executed when robot is disabled
     *
     * @param command the command to start
     * @param interruptible whether the command is interruptible
     * @return this trigger, so calls can be chained
     */
    public Trigger onTrue(final Runnable toRun, Subsystem... requirements) {
        return onTrue(new TestInstantCommand(toRun, requirements));
    }

    /**
     * Runs the given runnable when the trigger becomes inactive. Uses TestInstantCommand to allow the
     * runnable to be executed when robot is disabled
     *
     * @param toRun the runnable to run
     * @param requirements the required subsystems
     * @return this trigger, so calls can be chained
     */
    public Trigger onFalse(final Runnable toRun, Subsystem... requirements) {
        return onFalse(new TestInstantCommand(toRun, requirements));
    }
}
