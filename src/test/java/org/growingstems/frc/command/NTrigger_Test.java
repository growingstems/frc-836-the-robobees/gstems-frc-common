/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.frc.command;

import static org.junit.jupiter.api.Assertions.assertEquals;

import edu.wpi.first.wpilibj2.command.Command;
import org.growingstems.frc.test.CommandScheduleTest;
import org.growingstems.frc.test.TestInstantCommand;
import org.growingstems.frc.test.TestTrigger;
import org.junit.jupiter.api.Test;

public class NTrigger_Test extends CommandScheduleTest {
    int intTest = 0;
    boolean boolA = false;
    boolean boolB = false;
    boolean boolC = false;

    @Test
    public void threeTriggerTest() {
        TestTrigger trigA = new TestTrigger(() -> boolA);
        TestTrigger trigB = new TestTrigger(() -> boolB);
        TestTrigger trigC = new TestTrigger(() -> boolC);

        NTrigger ntr = new NTrigger(trigA, trigB, trigC);

        Command comT = new TestInstantCommand(() -> intTest = 1);
        Command comF = new TestInstantCommand(() -> intTest = 2);
        Command comInv = new TestInstantCommand(() -> intTest = 3);

        ntr.setCommands(comT, comF, comInv);

        assertEquals(0, intTest);
        step();
        assertEquals(0, intTest);
        boolA = true;
        step();
        assertEquals(1, intTest);
        boolA = false;
        step();
        assertEquals(1, intTest);
        boolB = true;
        step();
        assertEquals(2, intTest);
        boolC = true;
        step();
        assertEquals(3, intTest);
        boolB = false;
        step();
        assertEquals(3, intTest);
    }

    @Test
    public void stopTest() {
        TestTrigger trigA = new TestTrigger(() -> boolA);
        TestTrigger trigB = new TestTrigger(() -> boolB);

        NTrigger ntr = new NTrigger(trigA, trigB);

        Command upCom = new TestInstantCommand(() -> intTest++);
        Command downCom = new TestInstantCommand(() -> intTest--);
        Command zero = new TestInstantCommand(() -> intTest = 0);

        ntr.setCommands(upCom, downCom).setStopCommand(zero);

        assertEquals(0, intTest);
        step();
        assertEquals(0, intTest);
        boolA = true;
        step();
        assertEquals(1, intTest);
        boolA = false;
        step();
        assertEquals(0, intTest);
        boolB = true;
        step();
        assertEquals(-1, intTest);
        boolB = false;
        step();
        assertEquals(0, intTest);
        boolB = true;
        step();
        assertEquals(-1, intTest);
        boolA = true;
        step();
        assertEquals(0, intTest);
        boolA = false;
        step();
        assertEquals(-1, intTest);
        boolA = true;
        step();
        assertEquals(0, intTest);
        boolA = false;
        step();
        assertEquals(-1, intTest);
        boolA = true;
        step();
        assertEquals(0, intTest);
        boolA = false;
        step();
        assertEquals(-1, intTest);
        boolB = false;
        step();
        assertEquals(0, intTest);
    }
}
