/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.frc.util;

import static edu.wpi.first.units.Units.Amps;
import static edu.wpi.first.units.Units.Celsius;
import static edu.wpi.first.units.Units.Degrees;
import static edu.wpi.first.units.Units.DegreesPerSecond;
import static edu.wpi.first.units.Units.Fahrenheit;
import static edu.wpi.first.units.Units.FeetPerSecond;
import static edu.wpi.first.units.Units.Inches;
import static edu.wpi.first.units.Units.KilogramSquareMeters;
import static edu.wpi.first.units.Units.Kilograms;
import static edu.wpi.first.units.Units.Meters;
import static edu.wpi.first.units.Units.MetersPerSecond;
import static edu.wpi.first.units.Units.MetersPerSecondPerSecond;
import static edu.wpi.first.units.Units.Milliseconds;
import static edu.wpi.first.units.Units.Radians;
import static edu.wpi.first.units.Units.RadiansPerSecond;
import static edu.wpi.first.units.Units.RadiansPerSecondPerSecond;
import static edu.wpi.first.units.Units.Seconds;
import static edu.wpi.first.units.Units.Volts;
import static org.junit.jupiter.api.Assertions.assertEquals;

import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Rotation3d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.geometry.Translation3d;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;
import org.growingstems.math.Pose2dU;
import org.growingstems.math.Vector2dU;
import org.growingstems.math.Vector3dU;
import org.growingstems.measurements.Angle;
import org.growingstems.measurements.Measurements.Acceleration;
import org.growingstems.measurements.Measurements.AngularAcceleration;
import org.growingstems.measurements.Measurements.AngularVelocity;
import org.growingstems.measurements.Measurements.Current;
import org.growingstems.measurements.Measurements.Length;
import org.growingstems.measurements.Measurements.Mass;
import org.growingstems.measurements.Measurements.MomentOfInertia;
import org.growingstems.measurements.Measurements.Temperature;
import org.growingstems.measurements.Measurements.Time;
import org.growingstems.measurements.Measurements.Velocity;
import org.growingstems.measurements.Measurements.Voltage;
import org.growingstems.measurements.Measurements.VoltageRate;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class Convert_Test {
    protected static final double k_tolerance = 1.0e-9;
    protected static final double k_angularVelocityTolerance = 1.0e-8;
    protected static final double k_temperatureTolerance = 1.0e-4;
    protected static final List<Double> cartesianValues =
            List.of(0.0, 1.0, 0.0, 110.8, -1000.0, -0.01, 1234.5);
    protected static final List<Double> angleValues =
            List.of(0.0, 90.0, 180.0, 360.0, 720.0, -180.0, -540.0);

    protected static Stream<Arguments> miscTestData() {
        return angleValues.stream().map(Arguments::of);
    }

    protected static Stream<Arguments> vector2dTestData() {
        var args = new ArrayList<Arguments>();
        for (var x : cartesianValues) {
            for (var y : cartesianValues) {
                args.add(Arguments.of(x, y));
            }
        }
        return args.stream();
    }

    protected static Stream<Arguments> vector3dTestData() {
        var args = new ArrayList<Arguments>();
        for (var x : cartesianValues) {
            for (var y : cartesianValues) {
                for (var z : cartesianValues) {
                    args.add(Arguments.of(x, y, z));
                }
            }
        }
        return args.stream();
    }

    protected static Stream<Arguments> vector3dAngleTestData() {
        var args = new ArrayList<Arguments>();
        for (var x : angleValues) {
            for (var y : angleValues) {
                for (var z : angleValues) {
                    args.add(Arguments.of(x, y, z));
                }
            }
        }
        return args.stream();
    }

    protected static Stream<Arguments> poseTestData() {
        var args = new ArrayList<Arguments>();
        for (var x : cartesianValues) {
            for (var y : cartesianValues) {
                for (var theta : angleValues) {
                    args.add(Arguments.of(x, y, theta));
                }
            }
        }
        return args.stream();
    }

    // Vector3D/Translation3D
    @ParameterizedTest
    @MethodSource("vector3dTestData")
    public void vector3dToWpiTranslation3dTest(double x_in, double y_in, double z_in) {
        var gstemsVector =
                new Vector3dU<>(Length.inches(x_in), Length.inches(y_in), Length.inches(z_in));
        var actualVector = Convert.toWpi(gstemsVector, Length.Unit.INCHES);
        var expectedVector = new Translation3d(x_in, y_in, z_in);

        assertEquals(expectedVector.getX(), actualVector.getX(), k_tolerance);
        assertEquals(expectedVector.getY(), actualVector.getY(), k_tolerance);
        assertEquals(expectedVector.getZ(), actualVector.getZ(), k_tolerance);
    }

    @ParameterizedTest
    @MethodSource("vector3dTestData")
    public void vector3dFromWpiTranslation3dTest(double x_in, double y_in, double z_in) {
        var wpiVector = new Translation3d(x_in, y_in, z_in);
        var actualVector = Convert.fromWpi(wpiVector, Length.Unit.INCHES);
        var expectedVector =
                new Vector3dU<>(Length.inches(x_in), Length.inches(y_in), Length.inches(z_in));

        assertEquals(expectedVector.getX().asMeters(), actualVector.getX().asMeters(), k_tolerance);
        assertEquals(expectedVector.getY().asMeters(), actualVector.getY().asMeters(), k_tolerance);
        assertEquals(expectedVector.getZ().asMeters(), actualVector.getZ().asMeters(), k_tolerance);
    }

    @ParameterizedTest
    @MethodSource("vector3dTestData")
    public void vector3dToWpiMetersTest(double x_m, double y_m, double z_m) {
        var gstemsVector = new Vector3dU<>(Length.meters(x_m), Length.meters(y_m), Length.meters(z_m));
        var actualVector = Convert.toWpiMeters(gstemsVector);
        var expectedVector = new Translation3d(x_m, y_m, z_m);

        assertEquals(expectedVector.getX(), actualVector.getX(), k_tolerance);
        assertEquals(expectedVector.getY(), actualVector.getY(), k_tolerance);
        assertEquals(expectedVector.getZ(), actualVector.getZ(), k_tolerance);
    }

    @ParameterizedTest
    @MethodSource("vector3dTestData")
    public void vector3dFromWpiMetersTest(double x_m, double y_m, double z_m) {
        var wpiVector = new Translation3d(x_m, y_m, z_m);
        var actualVector = Convert.fromWpiMeters(wpiVector);
        var expectedVector =
                new Vector3dU<>(Length.meters(x_m), Length.meters(y_m), Length.meters(z_m));

        assertEquals(expectedVector.getX().asMeters(), actualVector.getX().asMeters(), k_tolerance);
        assertEquals(expectedVector.getY().asMeters(), actualVector.getY().asMeters(), k_tolerance);
        assertEquals(expectedVector.getZ().asMeters(), actualVector.getZ().asMeters(), k_tolerance);
    }

    // Vector3D/Rotation3D
    @ParameterizedTest
    @MethodSource("vector3dAngleTestData")
    public void vector3dToWpiRotation3dTest(double roll_deg, double pitch_deg, double yaw_deg) {
        var gstemsVector =
                new Vector3dU<>(Angle.degrees(roll_deg), Angle.degrees(pitch_deg), Angle.degrees(yaw_deg));
        var actualVector = Convert.toWpi(gstemsVector);
        var expectedVector =
                new Rotation3d(Degrees.of(roll_deg), Degrees.of(pitch_deg), Degrees.of(yaw_deg));

        assertEquals(
                expectedVector.getMeasureX().in(Radians),
                actualVector.getMeasureX().in(Radians),
                k_tolerance);
        assertEquals(
                expectedVector.getMeasureY().in(Radians),
                actualVector.getMeasureY().in(Radians),
                k_tolerance);
        assertEquals(
                expectedVector.getMeasureZ().in(Radians),
                actualVector.getMeasureZ().in(Radians),
                k_tolerance);
    }

    // The parameterized test "vector3dFromWpiRotation3dTest" fails due to the potential for several
    // equally correct answers. A few simple cases that succeed have been hard-coded in
    // "vector3dFromWpiRotation3dTestSimple" to ensure basic functionality, but the more complex
    // examples are currently not being tested.

    @Test
    public void vector3dFromWpiRotation3dTestSimple() {
        vector3dFromWpiRotation3dTest(0.0, 0.0, 0.0);
        vector3dFromWpiRotation3dTest(0.0, 0.0, 90.0);
        vector3dFromWpiRotation3dTest(0.0, 90.0, 0.0);
        vector3dFromWpiRotation3dTest(90.0, 0.0, 0.0);
        vector3dFromWpiRotation3dTest(90.0, 0.0, 90.0);
    }

    @ParameterizedTest
    @MethodSource("vector3dAngleTestData")
    @Disabled
    public void vector3dFromWpiRotation3dTest(double roll_deg, double pitch_deg, double yaw_deg) {
        var wpiVector =
                new Rotation3d(Degrees.of(roll_deg), Degrees.of(pitch_deg), Degrees.of(yaw_deg));
        var actualVector = Convert.fromWpi(wpiVector);
        var expectedVector =
                new Vector3dU<>(Angle.degrees(roll_deg), Angle.degrees(pitch_deg), Angle.degrees(yaw_deg));

        assertEquals(expectedVector.getX().asRadians(), actualVector.getX().asRadians(), k_tolerance);
        assertEquals(expectedVector.getY().asRadians(), actualVector.getY().asRadians(), k_tolerance);
        assertEquals(expectedVector.getZ().asRadians(), actualVector.getZ().asRadians(), k_tolerance);
    }

    // Pose2D
    @ParameterizedTest
    @MethodSource("poseTestData")
    public void pose2dFromWpiTest(double x_in, double y_in, double angle_deg) {
        var wpiPose = new Pose2d(x_in, y_in, new Rotation2d(Math.toRadians(angle_deg)));
        var actualPose = Convert.fromWpi(wpiPose, Length.Unit.INCHES);
        var expectedPose =
                new Pose2dU<>(Length.inches(x_in), Length.inches(y_in), Angle.degrees(angle_deg));

        assertEquals(expectedPose.getX().asMeters(), actualPose.getX().asMeters(), k_tolerance);
        assertEquals(expectedPose.getY().asMeters(), actualPose.getY().asMeters(), k_tolerance);
        assertEquals(
                expectedPose.getRotation().asRadians(), actualPose.getRotation().asRadians(), k_tolerance);
    }

    @ParameterizedTest
    @MethodSource("poseTestData")
    public void pose2dToWpiTest(double x_in, double y_in, double angle_deg) {
        var gstemsPose =
                new Pose2dU<>(Length.inches(x_in), Length.inches(y_in), Angle.degrees(angle_deg));
        var actualPose = Convert.toWpi(gstemsPose, Length.Unit.INCHES);
        var expectedPose = new Pose2d(x_in, y_in, new Rotation2d(Math.toRadians(angle_deg)));

        assertEquals(expectedPose.getX(), actualPose.getX(), k_tolerance);
        assertEquals(expectedPose.getY(), actualPose.getY(), k_tolerance);
        assertEquals(
                expectedPose.getRotation().getRadians(),
                actualPose.getRotation().getRadians(),
                k_tolerance);
    }

    @ParameterizedTest
    @MethodSource("poseTestData")
    public void pose2dFromWpiMetersTest(double x_m, double y_m, double angle_deg) {
        var wpiPose = new Pose2d(x_m, y_m, new Rotation2d(Math.toRadians(angle_deg)));
        var actualPose = Convert.fromWpiMeters(wpiPose);
        var expectedPose =
                new Pose2dU<>(Length.meters(x_m), Length.meters(y_m), Angle.degrees(angle_deg));

        assertEquals(expectedPose.getX().asMeters(), actualPose.getX().asMeters(), k_tolerance);
        assertEquals(expectedPose.getY().asMeters(), actualPose.getY().asMeters(), k_tolerance);
        assertEquals(
                expectedPose.getRotation().asRadians(), actualPose.getRotation().asRadians(), k_tolerance);
    }

    @ParameterizedTest
    @MethodSource("poseTestData")
    public void pose2dToWpiMetersTest(double x_m, double y_m, double angle_deg) {
        var gstemsPose =
                new Pose2dU<>(Length.meters(x_m), Length.meters(y_m), Angle.degrees(angle_deg));
        var actualPose = Convert.toWpiMeters(gstemsPose);
        var expectedPose = new Pose2d(x_m, y_m, new Rotation2d(Math.toRadians(angle_deg)));

        assertEquals(expectedPose.getX(), actualPose.getX(), k_tolerance);
        assertEquals(expectedPose.getY(), actualPose.getY(), k_tolerance);
        assertEquals(
                expectedPose.getRotation().getRadians(),
                actualPose.getRotation().getRadians(),
                k_tolerance);
    }

    // Vector2D/Translation2D
    @ParameterizedTest
    @MethodSource("vector2dTestData")
    public void vector2dToWpiTest(double x_in, double y_in) {
        var gstemsVector = new Vector2dU<>(Length.inches(x_in), Length.inches(y_in));
        var actualVector = Convert.toWpi(gstemsVector, Length.Unit.INCHES);
        var expectedVector = new Translation2d(x_in, y_in);

        assertEquals(expectedVector.getX(), actualVector.getX(), k_tolerance);
        assertEquals(expectedVector.getY(), actualVector.getY(), k_tolerance);
    }

    @ParameterizedTest
    @MethodSource("vector2dTestData")
    public void vector2dFromWpiTest(double x_in, double y_in) {
        var wpiVector = new Translation2d(x_in, y_in);
        var actualVector = Convert.fromWpi(wpiVector, Length.Unit.INCHES);
        var expectedVector = new Vector2dU<>(Length.inches(x_in), Length.inches(y_in));

        assertEquals(expectedVector.getX().asMeters(), actualVector.getX().asMeters(), k_tolerance);
        assertEquals(expectedVector.getY().asMeters(), actualVector.getY().asMeters(), k_tolerance);
    }

    @ParameterizedTest
    @MethodSource("vector2dTestData")
    public void vector2dToWpiMetersTest(double x_m, double y_m) {
        var gstemsVector = new Vector2dU<>(Length.meters(x_m), Length.meters(y_m));
        var actualVector = Convert.toWpiMeters(gstemsVector);
        var expectedVector = new Translation2d(x_m, y_m);

        assertEquals(expectedVector.getX(), actualVector.getX(), k_tolerance);
        assertEquals(expectedVector.getY(), actualVector.getY(), k_tolerance);
    }

    @ParameterizedTest
    @MethodSource("vector2dTestData")
    public void vector2dFromWpiMetersTest(double x_m, double y_m) {
        var wpiVector = new Translation2d(x_m, y_m);
        var actualVector = Convert.fromWpiMeters(wpiVector);
        var expectedVector = new Vector2dU<>(Length.meters(x_m), Length.meters(y_m));

        assertEquals(expectedVector.getX().asMeters(), actualVector.getX().asMeters(), k_tolerance);
        assertEquals(expectedVector.getY().asMeters(), actualVector.getY().asMeters(), k_tolerance);
    }

    // Length
    @ParameterizedTest
    @MethodSource("vector2dTestData")
    public void lengthFromWpiTest(double length_in) {
        var wpiLength = Inches.of(length_in);
        var actualLength = Convert.fromWpi(wpiLength);
        var expectedLength = Length.inches(length_in);

        assertEquals(expectedLength.asMeters(), actualLength.asMeters(), k_tolerance);
    }

    @ParameterizedTest
    @MethodSource("vector2dTestData")
    public void lengthToWpiTest(double length_in) {
        var gstemsLength = Length.inches(length_in);
        var actualLength = Convert.toWpi(gstemsLength);
        var expectedLength = Inches.of(length_in);

        assertEquals(expectedLength.in(Meters), actualLength.in(Meters), k_tolerance);
    }

    // Angle
    @ParameterizedTest
    @MethodSource("miscTestData")
    public void angleFromWpiTest(double angle_deg) {
        var wpiAngle = Degrees.of(angle_deg);
        var actualAngle = Convert.fromWpi(wpiAngle);
        var expectedAngle = Angle.degrees(angle_deg);

        assertEquals(expectedAngle.asRadians(), actualAngle.asRadians(), k_tolerance);
    }

    @ParameterizedTest
    @MethodSource("miscTestData")
    public void angleToWpiTest(double angle_deg) {
        var gstemsAngle = Angle.degrees(angle_deg);
        var actualAngle = Convert.toWpi(gstemsAngle);
        var expectedAngle = Degrees.of(angle_deg);

        assertEquals(expectedAngle.in(Radians), actualAngle.in(Radians), k_tolerance);
    }

    // Angle - Rotation2d
    @ParameterizedTest
    @MethodSource("miscTestData")
    public void angleFromRotation2dWpiTest(double angle_deg) {
        var wpiAngle = new Rotation2d(Math.toRadians(angle_deg));
        var actualAngle = Convert.fromWpi(wpiAngle);
        var expectedAngle = Angle.degrees(angle_deg);

        assertEquals(expectedAngle.asRadians(), actualAngle.asRadians(), k_tolerance);
    }

    @ParameterizedTest
    @MethodSource("miscTestData")
    public void angleToRotation2dWpiTest(double angle_deg) {
        var gstemsAngle = Angle.degrees(angle_deg);
        var actualAngle = Convert.toRotation2d(gstemsAngle);
        var expectedAngle = new Rotation2d(Math.toRadians(angle_deg));

        assertEquals(expectedAngle.getRadians(), actualAngle.getRadians(), k_tolerance);
    }

    // Voltage
    @ParameterizedTest
    @MethodSource("miscTestData")
    public void voltageFromWpiTest(double voltage_V) {
        var wpiVoltage = Volts.of(voltage_V);
        var actualVoltage = Convert.fromWpi(wpiVoltage);
        var expectedVoltage = Voltage.volts(voltage_V);

        assertEquals(expectedVoltage.asVolts(), actualVoltage.asVolts(), k_tolerance);
    }

    @ParameterizedTest
    @MethodSource("miscTestData")
    public void voltageToWpiTest(double voltage_V) {
        var gstemsVoltage = Voltage.volts(voltage_V);
        var actualVoltage = Convert.toWpi(gstemsVoltage);
        var expectedVoltage = Volts.of(voltage_V);

        assertEquals(expectedVoltage.in(Volts), actualVoltage.in(Volts), k_tolerance);
    }

    // Current
    @ParameterizedTest
    @MethodSource("miscTestData")
    public void currentFromWpiTest(double Current_V) {
        var wpiCurrent = Amps.of(Current_V);
        var actualCurrent = Convert.fromWpi(wpiCurrent);
        var expectedCurrent = Current.amps(Current_V);

        assertEquals(expectedCurrent.asAmps(), actualCurrent.asAmps(), k_tolerance);
    }

    @ParameterizedTest
    @MethodSource("miscTestData")
    public void currentToWpiTest(double current_A) {
        var gstemsCurrent = Current.amps(current_A);
        var actualCurrent = Convert.toWpi(gstemsCurrent);
        var expectedCurrent = Amps.of(current_A);

        assertEquals(expectedCurrent.in(Amps), actualCurrent.in(Amps), k_tolerance);
    }

    // Angular Velocity
    @ParameterizedTest
    @MethodSource("miscTestData")
    public void angularVelocityFromWpiTest(double angularVelocity_dps) {
        var wpiAngularVelocity = DegreesPerSecond.of(angularVelocity_dps);
        var actualAngularVelocity = Convert.fromWpi(wpiAngularVelocity);
        var expectedAngularVelocity = AngularVelocity.degreesPerSecond(angularVelocity_dps);

        assertEquals(
                expectedAngularVelocity.asRadiansPerSecond(),
                actualAngularVelocity.asRadiansPerSecond(),
                k_angularVelocityTolerance);
    }

    @ParameterizedTest
    @MethodSource("miscTestData")
    public void angularVelocityToWpiTest(double AngularVelocity_V) {
        var gstemsAngularVelocity = AngularVelocity.degreesPerSecond(AngularVelocity_V);
        var actualAngularVelocity = Convert.toWpi(gstemsAngularVelocity);
        var expectedAngularVelocity = DegreesPerSecond.of(AngularVelocity_V);

        assertEquals(
                expectedAngularVelocity.in(RadiansPerSecond),
                actualAngularVelocity.in(RadiansPerSecond),
                k_angularVelocityTolerance);
    }

    // Time
    @ParameterizedTest
    @MethodSource("miscTestData")
    public void timeeFromWpiTest(double time_ms) {
        var wpiTime = Milliseconds.of(time_ms);
        var actualTime = Convert.fromWpi(wpiTime);
        var expectedTime = Time.milliseconds(time_ms);

        assertEquals(expectedTime.getBaseValue(), actualTime.getBaseValue(), k_tolerance);
    }

    @ParameterizedTest
    @MethodSource("miscTestData")
    public void timeToWpiTest(double time_ms) {
        var gstemsTime = Time.milliseconds(time_ms);
        var actualTime = Convert.toWpi(gstemsTime);
        var expectedTime = Milliseconds.of(time_ms);

        assertEquals(expectedTime.in(Seconds), actualTime.in(Seconds), k_tolerance);
    }

    // Velocity
    @ParameterizedTest
    @MethodSource("miscTestData")
    public void velocityFromWpiTest(double velocity_fps) {
        var wpiVelocity = FeetPerSecond.of(velocity_fps);
        var actualVelocity = Convert.fromWpi(wpiVelocity);
        var expectedVelocity = Velocity.feetPerSecond(velocity_fps);

        assertEquals(
                expectedVelocity.asMetersPerSecond(), actualVelocity.asMetersPerSecond(), k_tolerance);
    }

    @ParameterizedTest
    @MethodSource("miscTestData")
    public void VelocityToWpiTest(double velocity_fps) {
        var gstemsVelocity = Velocity.feetPerSecond(velocity_fps);
        var actualVelocity = Convert.toWpi(gstemsVelocity);
        var expectedVelocity = FeetPerSecond.of(velocity_fps);

        assertEquals(
                expectedVelocity.in(MetersPerSecond), actualVelocity.in(MetersPerSecond), k_tolerance);
    }

    // Voltage Rate
    @ParameterizedTest
    @MethodSource("miscTestData")
    public void voltageRateFromWpiTest(double voltage_vps) {
        var wpiVoltageRate = Volts.of(voltage_vps).per(Seconds);
        var actualVoltageRate = Convert.fromWpi(wpiVoltageRate);
        var expectedVoltageRate = VoltageRate.voltsPerSecond(voltage_vps);

        assertEquals(
                expectedVoltageRate.asVoltsPerSecond(), actualVoltageRate.asVoltsPerSecond(), k_tolerance);
    }

    @ParameterizedTest
    @MethodSource("miscTestData")
    public void voltageRateToWpiTest(double voltage_vps) {
        var gstemsVoltageRate = VoltageRate.voltsPerSecond(voltage_vps);
        var actualVoltageRate = Convert.toWpi(gstemsVoltageRate);
        var expectedVoltageRate = Volts.of(voltage_vps).per(Seconds);

        assertEquals(
                expectedVoltageRate.baseUnitMagnitude(),
                actualVoltageRate.baseUnitMagnitude(),
                k_tolerance);
    }

    // Termperature
    @ParameterizedTest
    @MethodSource("miscTestData")
    public void temperatureFromWpiTest(double temperature_F) {
        var wpiTemperature = Fahrenheit.of(temperature_F);
        var actualTemperature = Convert.fromWpi(wpiTemperature);
        var expectedTemperature = Temperature.fahrenheit(temperature_F);

        assertEquals(
                expectedTemperature.asCelsius(), actualTemperature.asCelsius(), k_temperatureTolerance);
    }

    @ParameterizedTest
    @MethodSource("miscTestData")
    public void temperatureToWpiTest(double temperature_F) {
        var gstemsTemperature = Temperature.fahrenheit(temperature_F);
        var actualTemperature = Convert.toWpi(gstemsTemperature);
        var expectedTemperature = Fahrenheit.of(temperature_F);

        assertEquals(
                expectedTemperature.in(Celsius), actualTemperature.in(Celsius), k_temperatureTolerance);
    }

    // Acceleration
    @ParameterizedTest
    @MethodSource("miscTestData")
    public void accelerationFromWpiTest(double acceleration_mps2) {
        var wpiAcceleration = MetersPerSecondPerSecond.of(acceleration_mps2);
        var actualAcceleration = Convert.fromWpi(wpiAcceleration);
        var expectedAcceleration = Acceleration.metersPerSecondSquared(acceleration_mps2);

        assertEquals(
                expectedAcceleration.asMetersPerSecondSquared(),
                actualAcceleration.asMetersPerSecondSquared(),
                k_tolerance);
    }

    @ParameterizedTest
    @MethodSource("miscTestData")
    public void accelerationToWpiTest(double acceleration_mps2) {
        var gstemsAcceleration = Acceleration.metersPerSecondSquared(acceleration_mps2);
        var actualAcceleration = Convert.toWpi(gstemsAcceleration);
        var expectedAcceleration = MetersPerSecondPerSecond.of(acceleration_mps2);

        assertEquals(
                expectedAcceleration.in(MetersPerSecondPerSecond),
                actualAcceleration.in(MetersPerSecondPerSecond),
                k_tolerance);
    }

    // Angular Acceleration
    @ParameterizedTest
    @MethodSource("miscTestData")
    public void angularAccelerationFromWpiTest(double angularAcceleration_radps2) {
        var wpiAngularAcceleration = RadiansPerSecondPerSecond.of(angularAcceleration_radps2);
        var actualAngularAcceleration = Convert.fromWpi(wpiAngularAcceleration);
        var expectedAngularAcceleration =
                AngularAcceleration.radiansPerSecondSquared(angularAcceleration_radps2);

        assertEquals(
                expectedAngularAcceleration.asRadiansPerSecondSquared(),
                actualAngularAcceleration.asRadiansPerSecondSquared(),
                k_tolerance);
    }

    @ParameterizedTest
    @MethodSource("miscTestData")
    public void angularAccelerationToWpiTest(double angularAcceleration_radps2) {
        var gstemsAngularAcceleration =
                AngularAcceleration.radiansPerSecondSquared(angularAcceleration_radps2);
        var actualAngularAcceleration = Convert.toWpi(gstemsAngularAcceleration);
        var expectedAngularAcceleration = RadiansPerSecondPerSecond.of(angularAcceleration_radps2);

        assertEquals(
                expectedAngularAcceleration.in(RadiansPerSecondPerSecond),
                actualAngularAcceleration.in(RadiansPerSecondPerSecond),
                k_tolerance);
    }

    // Mass
    @ParameterizedTest
    @MethodSource("miscTestData")
    public void massFromWpiTest(double mass_kg) {
        var wpiMass = Kilograms.of(mass_kg);
        var actualMass = Convert.fromWpi(wpiMass);
        var expectedMass = Mass.kilograms(mass_kg);

        assertEquals(expectedMass.asKilograms(), actualMass.asKilograms(), k_tolerance);
    }

    @ParameterizedTest
    @MethodSource("miscTestData")
    public void massToWpiTest(double mass_kg) {
        var gstemsMass = Mass.kilograms(mass_kg);
        var actualMass = Convert.toWpi(gstemsMass);
        var expectedMass = Kilograms.of(mass_kg);

        assertEquals(expectedMass.in(Kilograms), actualMass.in(Kilograms), k_tolerance);
    }

    // Moment of Inertia
    @ParameterizedTest
    @MethodSource("miscTestData")
    public void momentOfInertiaFromWpiTest(double momentOfInertia_kgm2) {
        var wpiMomentOfInertia = KilogramSquareMeters.of(momentOfInertia_kgm2);
        var actualMomentOfInertia = Convert.fromWpi(wpiMomentOfInertia);
        var expectedMomentOfInertia = MomentOfInertia.kilogramMetersSquared(momentOfInertia_kgm2);

        assertEquals(
                expectedMomentOfInertia.asKilogramMetersSquared(),
                actualMomentOfInertia.asKilogramMetersSquared(),
                k_tolerance);
    }

    @ParameterizedTest
    @MethodSource("miscTestData")
    public void momentOfInertiaToWpiTest(double momentOfInertia_kgm2) {
        var gstemsMomentOfInertia = MomentOfInertia.kilogramMetersSquared(momentOfInertia_kgm2);
        var actualMomentOfInertia = Convert.toWpi(gstemsMomentOfInertia);
        var expectedMomentOfInertia = KilogramSquareMeters.of(momentOfInertia_kgm2);

        assertEquals(
                expectedMomentOfInertia.in(KilogramSquareMeters),
                actualMomentOfInertia.in(KilogramSquareMeters),
                k_tolerance);
    }
}
