package org.growingstems.frc.actuators;

import org.growingstems.math.RangeU;
import org.growingstems.measurements.Angle;
import org.growingstems.measurements.Measurements.Acceleration;
import org.growingstems.measurements.Measurements.AngularAcceleration;
import org.growingstems.measurements.Measurements.AngularVelocity;
import org.growingstems.measurements.Measurements.Length;
import org.growingstems.measurements.Measurements.Velocity;
import org.junit.jupiter.api.Test;

public class DummyCtreActuator_Test {
    @Test
    public void construction() {
        // The point of this test is to show that the generic type info is correct and meets the rather
        // strict requirements.
        // It does also show that the constructors don't crash.
        @SuppressWarnings("unused")
        DummyCtreActuator<Length, Velocity, Acceleration> linearActuator =
                new DummyCtreActuator<>(Length.inches(1.0));

        @SuppressWarnings("unused")
        DummyCtreActuator<Angle, AngularVelocity, AngularAcceleration> angularActuator =
                new DummyCtreActuator<>(
                        Angle.degrees(1.0),
                        Angle.ZERO,
                        new RangeU<>(Angle.ZERO, Angle.TWO_PI),
                        new RangeU<>(AngularVelocity.ZERO, AngularVelocity.degreesPerSecond(1.0)));
    }
}
