/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.frc.command;

import edu.wpi.first.wpilibj.event.EventLoop;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import edu.wpi.first.wpilibj2.command.button.Trigger;
import java.util.ArrayDeque;
import java.util.function.BooleanSupplier;
import org.apache.commons.math3.exception.DimensionMismatchException;

/**
 * Combines multiple Triggers to schedule Commands according to the following behavior: <br>
 *
 * <ul>
 *   <li>Each Trigger has a corresponding Command.
 *   <li>When a Trigger activates, its corresponding Command is scheduled.
 *   <li>When a Trigger deactivates, its corresponding Command is unscheduled.
 *       <ul>
 *         <li>If all Triggers are inactive, the Stop Command is scheduled.
 *       </ul>
 *   <li>The same Command will not be scheduled twice in a row.
 *   <li>The Stop Command is optional. If not provided, no Command will be scheduled in place.
 * </ul>
 */
public class NTrigger {

    private boolean m_forceCancel;

    // Triggers
    private Trigger[] m_triggers;

    // Commands to run when triggers occur
    private Command[] m_commands;
    private Command m_stopCommand = null;

    // State data - Stores the last known state of the triggers as well as the
    // currently running command.
    private ArrayDeque<Command> m_pressCheck = new ArrayDeque<Command>();
    private Command m_lastCommand = null;

    private final EventLoop m_loop;

    /**
     * Creates an NTrigger using Trigger objects and a custom event loop
     *
     * @param loop EventLoop within which to evaluate triggers
     * @param forceCancel Set to true to force cancel current command when new command scheduled
     * @param triggers The triggers to be used.
     */
    public NTrigger(EventLoop loop, boolean forceCancel, Trigger... triggers) {
        m_loop = loop;
        m_forceCancel = forceCancel;
        m_triggers = triggers;
        configureTriggers();
    }

    /**
     * Creates an NTrigger using Trigger objects
     *
     * @param forceCancel Set to true to force cancel current command when new command scheduled
     * @param triggers The triggers to be used.
     */
    public NTrigger(boolean forceCancel, Trigger... triggers) {
        this(CommandScheduler.getInstance().getDefaultButtonLoop(), forceCancel, triggers);
    }

    /**
     * Creates an NTrigger using Trigger objects.
     *
     * <p>Default behavior is to not force cancel commands.
     *
     * @param triggers The triggers to be used.
     */
    public NTrigger(Trigger... triggers) {
        this(false, triggers);
    }

    /**
     * Creates an NTrigger using BooleanSupplier objects and a custom event loop.
     *
     * @param loop EventLoop within which to evaluate triggers
     * @param forceCancel Set to true to force cancel current command when new command scheduled
     * @param suppliers The BooleanSuppliers that make the triggers.
     */
    public NTrigger(EventLoop loop, boolean forceCancel, BooleanSupplier[] suppliers) {
        m_loop = loop;
        m_forceCancel = forceCancel;
        m_triggers = new Trigger[suppliers.length];
        for (int i = 0; i < m_triggers.length; i++) {
            m_triggers[i] = new Trigger(suppliers[i]);
        }

        configureTriggers();
    }

    /**
     * Creates an NTrigger using BooleanSupplier objects.
     *
     * @param forceCancel Set to true to force cancel current command when new command scheduled
     * @param suppliers The BooleanSuppliers that make the triggers.
     */
    public NTrigger(boolean forceCancel, BooleanSupplier[] suppliers) {
        this(CommandScheduler.getInstance().getDefaultButtonLoop(), forceCancel, suppliers);
    }

    /**
     * Creates an NTrigger using BooleanSupplier objects.
     *
     * <p>Default behavior is to not force cancel commands.
     *
     * @param suppliers the suppliers to turn into triggers.
     */
    public NTrigger(BooleanSupplier[] suppliers) {
        this(false, suppliers);
    }

    /**
     * Accesses the getAsBoolean method of the specified trigger
     *
     * @param index the index of the Trigger to get
     * @return The return value of the specified Trigger's get method
     */
    public boolean getAsBoolean(int index) {
        return m_triggers[index].getAsBoolean();
    }

    /**
     * Links a Trigger to a command.
     *
     * @param command Command to be linked with a Trigger
     * @param index Trigger to link the command to
     * @throws ArrayIndexOutOfBoundsException when passed index is outside of the array of triggers
     * @return This NTrigger
     */
    public NTrigger setCommand(int index, Command command) throws ArrayIndexOutOfBoundsException {
        if (index > m_commands.length) {
            throw new ArrayIndexOutOfBoundsException("Passed index exceeds number of triggers. "
                    + "Index passed: " + index + ", number of triggers: " + m_commands.length);
        }

        m_commands[index] = command;
        return this;
    }

    /**
     * Links multiple Triggers to commands.
     *
     * <p>Commands are assigned to Triggers based on the order the Triggers were passed in to the
     * constructor.
     *
     * @param commands the commands to be linked to triggers.
     * @throws DimensionMismatchException when the number of passed Commands is larger than the number
     *     of Triggers.
     * @return This NTrigger
     */
    public NTrigger setCommands(Command... commands) throws DimensionMismatchException {
        if (commands.length > m_commands.length) {
            throw new DimensionMismatchException(commands.length, m_triggers.length);
        }

        for (int i = 0; i < commands.length; i++) {
            m_commands[i] = commands[i];
        }

        return this;
    }

    /**
     * Sets the stop command
     *
     * @param command the stop command to be set
     * @return This NTrigger
     */
    public NTrigger setStopCommand(Command command) {
        m_stopCommand = command;
        return this;
    }

    /**
     * Update is called when a Trigger changes value. Update is responsible for determining if the
     * Trigger's change should cause a Command to be scheduled.
     *
     * @param index the index of the Trigger that changed value.
     * @param value the new value
     * @return This NTrigger
     */
    private NTrigger update(int index, boolean value) {
        if (value) {
            m_pressCheck.add(m_commands[index]);
        } else {
            m_pressCheck.removeFirstOccurrence(m_commands[index]);
        }

        Command runningCommand = m_pressCheck.peekLast();

        if (runningCommand == null) {
            runningCommand = m_stopCommand;
        }

        if (runningCommand == m_lastCommand) {
            return this;
        }

        if (m_lastCommand != null && m_forceCancel) {
            m_lastCommand.cancel();
        }

        m_lastCommand = runningCommand;

        if (m_lastCommand != null) {
            m_lastCommand.schedule();
        }

        return this;
    }

    private NTrigger configureTriggers() {
        for (int i = 0; i < m_triggers.length; i++) {
            final int o = i;

            // combine trigger presses into one event that occurs on the button binding loop, rather than
            // the command loop
            // The call to update will schedule commands if necessary
            m_loop.bind(new Runnable() {
                private boolean m_pressedLast = m_triggers[o].getAsBoolean();

                @Override
                public void run() {
                    boolean pressed = m_triggers[o].getAsBoolean();

                    if (!m_pressedLast && pressed) {
                        update(o, true);
                    } else if (m_pressedLast && !pressed) {
                        update(o, false);
                    }

                    m_pressedLast = pressed;
                }
            });
        }

        m_commands = new Command[m_triggers.length];

        return this;
    }
}
