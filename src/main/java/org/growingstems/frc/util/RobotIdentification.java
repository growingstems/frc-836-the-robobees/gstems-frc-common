/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.frc.util;

import java.util.List;

/** An identifiable robot for use with {@link RobotIdentifier}. */
public interface RobotIdentification {
    /**
     * Returns a list of MAC addresses for the robot. These MAC address strings should be 6 hex octets
     * with hyphens separating them, for example: {@code B8-C0-C0-0D-2A-5C}.
     *
     * @return list of MAC addresses for the given Robot
     */
    List<String> getMacAddresses();
}
