/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.frc.util;

import edu.wpi.first.wpilibj.RobotController;
import org.growingstems.measurements.Measurements.Time;
import org.growingstems.util.timer.TimeSource;
import org.growingstems.util.timer.Timer;

/**
 * {@link org.growingstems.util.timer.TimeSource} implementation using
 * {@link edu.wpi.first.wpilibj.RobotController#getFPGATime()}. Precision: 1 microsecond
 */
public class WpiTimeSource implements TimeSource {
    private static final TimeSource k_timeSource = new WpiTimeSource();

    /**
     * Gets the current time using {@link edu.wpi.first.wpilibj.RobotController#getFPGATime()}
     *
     * @return Current time according to the FPGA.
     */
    @Override
    public Time clockTime() {
        return Time.microseconds(RobotController.getFPGATime());
    }

    /**
     * Returns a static WPI Time Source.
     *
     * @return statically defined WPI Time Source
     */
    public static final TimeSource getTimeSource() {
        return k_timeSource;
    }

    /**
     * Generates a timer using a statically defined WPI Time Source.
     *
     * @return timer based on a statically defined WPI Time Source
     */
    public static final Timer generateTimer() {
        return getTimeSource().createTimer();
    }
}
