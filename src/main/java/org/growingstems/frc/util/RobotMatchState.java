/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.frc.util;

import edu.wpi.first.wpilibj.DriverStation;

/** The RobotMatchState class represents the current match state as read by the robot. */
public class RobotMatchState {
    /**
     * Represents all possible match modes that the robot might find itself in based direct
     * communication from the driver station.
     */
    public static enum MatchMode {
        /**
         * A field-level e-stop performed by FTA or by the driver via the E-Stop button. Also can be
         * performed by hitting spacebar on the driver station computer if the driver station is not
         * attached to an FMS.
         */
        EMERGENCY_STOPPED,
        /** The autonomous period, allowing autonomous code execution. */
        AUTO,
        /**
         * The tele-operated period, allowing driver control and prohibiting autonomous mode execution.
         */
        TELE,
        /**
         * An unofficial testing mode, selected by the user on the driver station computer when the
         * driver is not attached to an FMS.
         */
        TEST,
        /** A fallback, unknown state, when the robot is in none of the other states. */
        UNKNOWN
    }

    /**
     * Represents all possibly match states.<br>
     * <br>
     * This includes all possible match modes in their enabled and disabled state, except for
     * {@link MatchMode#EMERGENCY_STOPPED}, which cannot be enabled.
     */
    public static enum MatchState {
        /**
         * A field-level e-stop performed by FTA or by the driver via the E-Stop button. Also can be
         * performed by hitting spacebar on the driver station computer if the driver station is not
         * attached to an FMS.
         */
        EMERGENCY_STOPPED(MatchMode.EMERGENCY_STOPPED, false),
        /** The enabled autonomous period, allowing autonomous code execution. */
        AUTO_ENABLED(MatchMode.AUTO, true),
        /** The disabled autonomous period. */
        AUTO_DISABLED(MatchMode.AUTO, false),
        /**
         * The enabled tele-operated period, allowing driver control and prohibiting autonomous mode
         * execution.
         */
        TELE_ENABLED(MatchMode.TELE, true),
        /** The disabled tele-operated period. */
        TELE_DISABLED(MatchMode.TELE, false),
        /**
         * An unofficial enabled testing mode, selected by the user on the driver station computer when
         * the driver is not attached to an FMS.
         */
        TEST_ENABLED(MatchMode.TEST, true),
        /** An unofficial disabled testing mode. */
        TEST_DISABLED(MatchMode.TEST, false),
        /** A fallback, enabled unknown state, when the robot is in none of the other states. */
        UNKNOWN_ENABLED(MatchMode.UNKNOWN, true),
        /** A fallback, disabled unknown state, when the robot is in none of the other states. */
        UNKNOWN_DISABLED(MatchMode.UNKNOWN, false);

        /** The {@link MatchMode} associated with this state */
        public final MatchMode matchMode;
        /** True if the robot is enabled */
        public final boolean enabled;

        private MatchState(MatchMode matchMode, boolean enabled) {
            this.matchMode = matchMode;
            this.enabled = enabled;
        }
    }

    /**
     * Based on the Driver station's readings, this method returns the current match state.
     *
     * @return Returns the current match state.
     */
    public static MatchState getMatchState() {
        // estop
        if (DriverStation.isEStopped()) {
            return MatchState.EMERGENCY_STOPPED;
        }

        // auto
        if (DriverStation.isAutonomousEnabled()) {
            return MatchState.AUTO_ENABLED;
        }
        if (DriverStation.isAutonomous()) {
            return MatchState.AUTO_DISABLED;
        }

        // teleop
        if (DriverStation.isTeleopEnabled()) {
            return MatchState.TELE_ENABLED;
        }
        if (DriverStation.isTeleop()) {
            return MatchState.TELE_DISABLED;
        }

        // test (has to explicitly check isEnabled because there is not
        // testEnabled())
        if (DriverStation.isTest() && DriverStation.isEnabled()) {
            return MatchState.TEST_ENABLED;
        }
        if (DriverStation.isTest()) {
            return MatchState.TEST_DISABLED;
        }

        // somehow in an unknown mode
        if (DriverStation.isEnabled()) {
            return MatchState.UNKNOWN_ENABLED;
        }
        return MatchState.UNKNOWN_DISABLED;
    }
}
