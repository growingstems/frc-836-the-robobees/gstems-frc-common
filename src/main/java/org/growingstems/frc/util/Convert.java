/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.frc.util;

import static edu.wpi.first.units.Units.Amps;
import static edu.wpi.first.units.Units.Celsius;
import static edu.wpi.first.units.Units.KilogramSquareMeters;
import static edu.wpi.first.units.Units.Kilograms;
import static edu.wpi.first.units.Units.Meters;
import static edu.wpi.first.units.Units.MetersPerSecond;
import static edu.wpi.first.units.Units.MetersPerSecondPerSecond;
import static edu.wpi.first.units.Units.Radians;
import static edu.wpi.first.units.Units.RadiansPerSecond;
import static edu.wpi.first.units.Units.RadiansPerSecondPerSecond;
import static edu.wpi.first.units.Units.Seconds;
import static edu.wpi.first.units.Units.Volts;

import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Rotation3d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.geometry.Translation3d;
import edu.wpi.first.units.VoltageUnit;
import edu.wpi.first.units.measure.Distance;
import edu.wpi.first.units.measure.LinearVelocity;
import org.growingstems.math.Pose2dU;
import org.growingstems.math.Vector2dU;
import org.growingstems.math.Vector3dU;
import org.growingstems.measurements.Angle;
import org.growingstems.measurements.Measurements.Acceleration;
import org.growingstems.measurements.Measurements.AngularAcceleration;
import org.growingstems.measurements.Measurements.AngularVelocity;
import org.growingstems.measurements.Measurements.Current;
import org.growingstems.measurements.Measurements.Length;
import org.growingstems.measurements.Measurements.Mass;
import org.growingstems.measurements.Measurements.MomentOfInertia;
import org.growingstems.measurements.Measurements.Temperature;
import org.growingstems.measurements.Measurements.Time;
import org.growingstems.measurements.Measurements.Velocity;
import org.growingstems.measurements.Measurements.Voltage;
import org.growingstems.measurements.Measurements.VoltageRate;

/**
 * Utilities class that contains several static functions meant to help convert from WPI Lib's
 * classes into our own versions of the classes and vice-versa.
 */
public class Convert {
    // Vector3D/Translation3D
    /**
     * Converts from WPI's {@link Translation3d} into our equivalent unit based {@link Vector3dU}.
     *
     * @param translation3d WPI's {@link Translation3d}
     * @param lengthUnit the length unit that WPI's {@link Translation3d} was in
     * @return our unit based {@link Vector3dU}
     */
    public static Vector3dU<Length> fromWpi(Translation3d translation3d, Length.Unit lengthUnit) {
        return new Vector3dU<>(
                new Length(translation3d.getX(), lengthUnit),
                new Length(translation3d.getY(), lengthUnit),
                new Length(translation3d.getZ(), lengthUnit));
    }

    /**
     * Converts from our unit based {@link Vector3dU} into WPI's {@link Translation3d}.
     *
     * @param vector3d our unit based {@link Vector3dU}
     * @param lengthUnit the length unit to build WPI's {@link Translation3d} with
     * @return WPI's {@link Translation3d}
     */
    public static Translation3d toWpi(Vector3dU<Length> vector3d, Length.Unit lengthUnit) {
        return new Translation3d(
                vector3d.getX().getValue(lengthUnit),
                vector3d.getY().getValue(lengthUnit),
                vector3d.getZ().getValue(lengthUnit));
    }

    /**
     * Converts from WPI's {@link Translation3d} into our equivalent unit based {@link Vector3dU}.
     * Assumes that the WPI {@link Translation3d} that is given was stored in meters.
     *
     * @param translation3dMeters WPI's {@link Translation3d} in meters
     * @return our unit based {@link Vector3dU}
     */
    public static Vector3dU<Length> fromWpiMeters(Translation3d translation3dMeters) {
        return new Vector3dU<>(
                fromWpi(translation3dMeters.getMeasureX()),
                fromWpi(translation3dMeters.getMeasureY()),
                fromWpi(translation3dMeters.getMeasureZ()));
    }

    /**
     * Converts from our unit based {@link Vector3dU} into WPI's {@link Translation3d}, specifically
     * in meters.
     *
     * @param vector3d our unit based {@link Vector3dU}
     * @return WPI's {@link Translation3d} in meters
     */
    public static Translation3d toWpiMeters(Vector3dU<Length> vector3d) {
        return new Translation3d(
                toWpi(vector3d.getX()), toWpi(vector3d.getY()), toWpi(vector3d.getZ()));
    }

    // Vector3D/Rotation3d
    /**
     * Converts from WPI's {@link Rotation3d} into our equivalent unit based {@link Vector3dU},
     * represented as Roll/Pitch/Yaw.
     *
     * @param rotation3d WPI's {@link Rotation3d}
     * @return our unit based {@link Vector3dU}
     */
    public static Vector3dU<Angle> fromWpi(Rotation3d rotation3d) {
        return new Vector3dU<>(
                fromWpi(rotation3d.getMeasureX()),
                fromWpi(rotation3d.getMeasureY()),
                fromWpi(rotation3d.getMeasureZ()));
    }
    /**
     * Converts from our unit based {@link Vector3dU}, in Roll/Pitch/Yaw form, into WPI's
     * {@link Rotation3d}.
     *
     * @param vector3d our unit based {@link Vector3dU} in Roll/Pitch/Yaw form
     * @return WPI's {@link Rotation3d}
     */
    public static Rotation3d toWpi(Vector3dU<Angle> vector3d) {
        return new Rotation3d(toWpi(vector3d.getX()), toWpi(vector3d.getY()), toWpi(vector3d.getZ()));
    }

    // Pose2D
    /**
     * Converts from WPI's {@link edu.wpi.first.math.geometry.Pose2d} into our equivalent unit based
     * {@link Pose2dU}.
     *
     * @param pose2d WPI's {@link edu.wpi.first.math.geometry.Pose2d}
     * @param lengthUnit the length unit that WPI's {@link edu.wpi.first.math.geometry.Pose2d} was in
     * @return our unit based {@link Pose2dU}
     */
    public static Pose2dU<Length> fromWpi(
            edu.wpi.first.math.geometry.Pose2d pose2d, Length.Unit lengthUnit) {
        return new Pose2dU<>(
                new Length(pose2d.getX(), lengthUnit),
                new Length(pose2d.getY(), lengthUnit),
                fromWpi(pose2d.getRotation()));
    }

    /**
     * Converts from our unit based {@link Pose2dU} into WPI's
     * {@link edu.wpi.first.math.geometry.Pose2d}.
     *
     * @param pose our unit based {@link Pose2dU}
     * @param lengthUnit the length unit to use when building WPI's
     *     {@link edu.wpi.first.math.geometry.Pose2d}
     * @return WPI's {@link edu.wpi.first.math.geometry.Pose2d}
     */
    public static edu.wpi.first.math.geometry.Pose2d toWpi(
            Pose2dU<Length> pose, Length.Unit lengthUnit) {
        return new edu.wpi.first.math.geometry.Pose2d(
                pose.getX().getValue(lengthUnit),
                pose.getY().getValue(lengthUnit),
                toRotation2d(pose.getRotation()));
    }

    /**
     * Converts from WPI's {@link edu.wpi.first.math.geometry.Pose2d} into our unit based
     * {@link Pose2dU}. Assumes that the WPI {@link edu.wpi.first.math.geometry.Pose2d} that is given
     * was stored in meters.
     *
     * @param pose2dMeters WPI's {@link edu.wpi.first.math.geometry.Pose2d} in meters
     * @return our unit based {@link Pose2dU}
     */
    public static Pose2dU<Length> fromWpiMeters(edu.wpi.first.math.geometry.Pose2d pose2dMeters) {
        return new Pose2dU<>(
                fromWpi(pose2dMeters.getMeasureX()),
                fromWpi(pose2dMeters.getMeasureY()),
                fromWpi(pose2dMeters.getRotation()));
    }

    /**
     * Converts from our unit based {@link Pose2dU} into WPI's
     * {@link edu.wpi.first.math.geometry.Pose2d}, specifically in meters.
     *
     * @param pose our unit based {@link Pose2dU}
     * @return WPI's {@link edu.wpi.first.math.geometry.Pose2d} in meters
     */
    public static edu.wpi.first.math.geometry.Pose2d toWpiMeters(Pose2dU<Length> pose) {
        return new edu.wpi.first.math.geometry.Pose2d(
                toWpi(pose.getX()), toWpi(pose.getY()), toRotation2d(pose.getRotation()));
    }

    // Vector2D/Translation2D
    /**
     * Converts from WPI's {@link Translation2d} into our equivalent unit based {@link Vector2dU}.
     *
     * @param translation2d WPI's {@link Translation2d}
     * @param lengthUnit the length unit that WPI's {@link Translation2d} was in
     * @return our unit based {@link Vector2dU}
     */
    public static Vector2dU<Length> fromWpi(Translation2d translation2d, Length.Unit lengthUnit) {
        return new Vector2dU<>(
                new Length(translation2d.getX(), lengthUnit), new Length(translation2d.getY(), lengthUnit));
    }

    /**
     * Converts from our unit based {@link Vector2dU} into WPI's {@link Translation2d}.
     *
     * @param vector2d our unit based {@link Vector2dU}
     * @param lengthUnit the length unit to build WPI's {@link Translation2d} with
     * @return WPI's {@link Translation2d}
     */
    public static edu.wpi.first.math.geometry.Translation2d toWpi(
            Vector2dU<Length> vector2d, Length.Unit lengthUnit) {
        return new Translation2d(
                vector2d.getX().getValue(lengthUnit), vector2d.getY().getValue(lengthUnit));
    }

    /**
     * Converts from WPI's {@link Translation2d} into our equivalent unit based {@link Vector2dU}.
     * Assumes that the WPI {@link Translation2d} that is given was stored in meters.
     *
     * @param translation2dMeters WPI's {@link Translation2d} in meters
     * @return our unit based {@link Vector2dU}
     */
    public static Vector2dU<Length> fromWpiMeters(Translation2d translation2dMeters) {
        return new Vector2dU<>(
                fromWpi(translation2dMeters.getMeasureX()), fromWpi(translation2dMeters.getMeasureY()));
    }

    /**
     * Converts from our unit based {@link Vector2dU} into WPI's {@link Translation2d}, specifically
     * in meters.
     *
     * @param vector2d our unit based {@link Vector2dU}
     * @return WPI's {@link Translation2d} in meters
     */
    public static Translation2d toWpiMeters(Vector2dU<Length> vector2d) {
        return new Translation2d(toWpi(vector2d.getX()), toWpi(vector2d.getY()));
    }

    // Length
    /**
     * Converts from our {@link Length} unit into WPI's {Distance} unit.
     *
     * @param length our {@link Length} unit
     * @return WPI's {Distance}
     */
    public static Distance toWpi(Length length) {
        return Meters.of(length.asMeters());
    }

    /**
     * Converts from WPI's {@link Distance} unit into our equivalent {@link Length} unit.
     *
     * @param length WPI's {@link Distance}
     * @return our {@link Length} unit
     */
    public static Length fromWpi(Distance length) {
        return Length.meters(length.in(Meters));
    }

    // Angle
    /**
     * Converts from our {@link Angle} unit into WPI's {@link edu.wpi.first.units.measure.Angle} unit.
     *
     * @param angle our {@link Angle} unit
     * @return WPI's {@link edu.wpi.first.units.measure.Angle}
     */
    public static edu.wpi.first.units.measure.Angle toWpi(Angle angle) {
        return Radians.of(angle.asRadians());
    }

    /**
     * Converts from WPI's {@link edu.wpi.first.units.measure.Angle} unit into our equivalent
     * {@link Angle} unit.
     *
     * @param angle WPI's {@link edu.wpi.first.units.measure.Angle}
     * @return our {@link Angle} unit
     */
    public static Angle fromWpi(edu.wpi.first.units.measure.Angle angle) {
        return Angle.radians(angle.in(Radians));
    }

    // Angle - Rotation2d
    /**
     * Converts from our {@link Angle} unit into WPI's {@link Rotation2d}.
     *
     * @param angle our {@link Angle} unit
     * @return WPI's {@link Rotation2d}
     */
    public static Rotation2d toRotation2d(Angle angle) {
        return new Rotation2d(toWpi(angle));
    }

    /**
     * Converts from WPI's {@link Rotation2d} into our equivalent {@link Angle} unit.
     *
     * @param rotation2d WPI's {@link Rotation2d}
     * @return our {@link Angle} unit
     */
    public static Angle fromWpi(Rotation2d rotation2d) {
        return Angle.radians(rotation2d.getRadians());
    }

    // Voltage
    /**
     * Converts from our {@link Voltage} unit into WPI's {@link edu.wpi.first.units.measure.Voltage}
     * unit.
     *
     * @param voltage our {@link Voltage} unit
     * @return WPI's {@link edu.wpi.first.units.measure.Voltage}
     */
    public static edu.wpi.first.units.measure.Voltage toWpi(Voltage voltage) {
        return Volts.of(voltage.asVolts());
    }

    /**
     * Converts from WPI's {@link edu.wpi.first.units.measure.Voltage} unit into our equivalent
     * {@link Voltage} unit.
     *
     * @param voltage WPI's {@link edu.wpi.first.units.measure.Voltage}
     * @return our {@link Voltage} unit
     */
    public static Voltage fromWpi(edu.wpi.first.units.measure.Voltage voltage) {
        return Voltage.volts(voltage.in(Volts));
    }

    // Current
    /**
     * Converts from our {@link Current} unit into WPI's {@link edu.wpi.first.units.measure.Current}
     * unit.
     *
     * @param current our {@link Current} unit
     * @return WPI's {@link edu.wpi.first.units.measure.Current}
     */
    public static edu.wpi.first.units.measure.Current toWpi(Current current) {
        return Amps.of(current.asAmps());
    }

    /**
     * Converts from WPI's {@link edu.wpi.first.units.measure.Current} unit into our equivalent
     * {@link Current} unit.
     *
     * @param current WPI's {@link edu.wpi.first.units.measure.Current}
     * @return our {@link Current} unit
     */
    public static Current fromWpi(edu.wpi.first.units.measure.Current current) {
        return Current.amps(current.in(Amps));
    }

    // Angular Velocity
    /**
     * Converts from our {@link AngularVelocity} unit into WPI's
     * {@link edu.wpi.first.units.measure.AngularVelocity} unit.
     *
     * @param angularVelocity our {@link AngularVelocity} unit
     * @return WPI's {@link edu.wpi.first.units.measure.AngularVelocity}
     */
    public static edu.wpi.first.units.measure.AngularVelocity toWpi(AngularVelocity angularVelocity) {
        return RadiansPerSecond.of(angularVelocity.asRadiansPerSecond());
    }

    /**
     * Converts from WPI's {@link edu.wpi.first.units.measure.AngularVelocity} unit into our
     * equivalent {@link AngularVelocity} unit.
     *
     * @param angularVelocity WPI's {@link edu.wpi.first.units.measure.AngularVelocity}
     * @return our {@link AngularVelocity} unit
     */
    public static AngularVelocity fromWpi(
            edu.wpi.first.units.measure.AngularVelocity angularVelocity) {
        return AngularVelocity.radiansPerSecond(angularVelocity.in(RadiansPerSecond));
    }

    // Time
    /**
     * Converts from our {@link Time} unit into WPI's {@link edu.wpi.first.units.measure.Time} unit.
     *
     * @param time our {@link Time} unit
     * @return WPI's {@link edu.wpi.first.units.measure.Time}
     */
    public static edu.wpi.first.units.measure.Time toWpi(Time time) {
        return Seconds.of(time.asSeconds());
    }

    /**
     * Converts from WPI's {@link edu.wpi.first.units.measure.Time} unit into our equivalent
     * {@link Time} unit.
     *
     * @param time WPI's {@link edu.wpi.first.units.measure.Time}
     * @return our {@link Time} unit
     */
    public static Time fromWpi(edu.wpi.first.units.measure.Time time) {
        return Time.seconds(time.in(Seconds));
    }

    // Velocity
    /**
     * Converts from our {@link Velocity} unit into WPI's {@link LinearVelocity} unit.
     *
     * @param velocity our {@link Velocity} unit
     * @return WPI's {@link LinearVelocity}
     */
    public static LinearVelocity toWpi(Velocity velocity) {
        return MetersPerSecond.of(velocity.asMetersPerSecond());
    }

    /**
     * Converts from WPI's {@link LinearVelocity} unit into our equivalent {@link Velocity} unit.
     *
     * @param velocity WPI's {@link LinearVelocity}
     * @return our {@link Velocity} unit
     */
    public static Velocity fromWpi(LinearVelocity velocity) {
        return Velocity.metersPerSecond(velocity.in(MetersPerSecond));
    }

    // Voltage Rate
    /**
     * Converts from our {@link VoltageRate} unit into WPI's {@code Velocity<VoltageUnit>} unit.
     *
     * @param voltageRate our {@link VoltageRate} unit
     * @return WPI's {@code Velocity<VoltageUnit>}
     */
    public static edu.wpi.first.units.measure.Velocity<VoltageUnit> toWpi(VoltageRate voltageRate) {
        return Volts.of(voltageRate.mul(Time.seconds(1.0)).asVolts()).per(Seconds);
    }

    /**
     * Converts from WPI's {@code Velocity<VoltageUnit>} unit into our equivalent {@link VoltageRate}
     * unit.
     *
     * @param voltageRate WPI's {@code Velocity<VoltageUnit>}
     * @return our {@link VoltageRate} unit
     */
    public static VoltageRate fromWpi(edu.wpi.first.units.measure.Velocity<VoltageUnit> voltageRate) {
        return Voltage.volts(voltageRate.times(Seconds.of(1.0)).in(Volts)).div(Time.seconds(1.0));
    }

    // Temperature
    /**
     * Converts from our {@link Temperature} unit into WPI's
     * {@link edu.wpi.first.units.measure.Temperature} unit.
     *
     * @param temperature our {@link Temperature} unit
     * @return WPI's {@link edu.wpi.first.units.measure.Temperature}
     */
    public static edu.wpi.first.units.measure.Temperature toWpi(Temperature temperature) {
        return Celsius.of(temperature.asCelsius());
    }

    /**
     * Converts from WPI's {@link edu.wpi.first.units.measure.Temperature} unit into our equivalent
     * {@link Temperature} unit.
     *
     * @param temperature WPI's {@link edu.wpi.first.units.measure.Temperature}
     * @return our {@link Temperature} unit
     */
    public static Temperature fromWpi(edu.wpi.first.units.measure.Temperature temperature) {
        return Temperature.celsius(temperature.in(Celsius));
    }

    // Acceleration
    /**
     * Converts from our {@link Acceleration} unit into WPI's
     * {@link edu.wpi.first.units.measure.LinearAcceleration} unit.
     *
     * @param acceleration our {@link Acceleration} unit
     * @return WPI's {@link edu.wpi.first.units.measure.LinearAcceleration}
     */
    public static edu.wpi.first.units.measure.LinearAcceleration toWpi(Acceleration acceleration) {
        return MetersPerSecondPerSecond.of(acceleration.asMetersPerSecondSquared());
    }

    /**
     * Converts from WPI's {@link edu.wpi.first.units.measure.LinearAcceleration} unit into our
     * equivalent {@link Acceleration} unit.
     *
     * @param acceleration WPI's {@link edu.wpi.first.units.measure.LinearAcceleration}
     * @return our {@link Acceleration} unit
     */
    public static Acceleration fromWpi(edu.wpi.first.units.measure.LinearAcceleration acceleration) {
        return Acceleration.metersPerSecondSquared(acceleration.in(MetersPerSecondPerSecond));
    }

    // Angular Acceleration
    /**
     * Converts from our {@link AngularAcceleration} unit into WPI's
     * {@link edu.wpi.first.units.measure.AngularAcceleration} unit.
     *
     * @param angularAcceleration our {@link AngularAcceleration} unit
     * @return WPI's {@link edu.wpi.first.units.measure.AngularAcceleration}
     */
    public static edu.wpi.first.units.measure.AngularAcceleration toWpi(
            AngularAcceleration angularAcceleration) {
        return RadiansPerSecondPerSecond.of(angularAcceleration.asRadiansPerSecondSquared());
    }

    /**
     * Converts from WPI's {@link edu.wpi.first.units.measure.AngularAcceleration} unit into our
     * equivalent {@link AngularAcceleration} unit.
     *
     * @param angularAcceleration WPI's {@link edu.wpi.first.units.measure.AngularAcceleration}
     * @return our {@link AngularAcceleration} unit
     */
    public static AngularAcceleration fromWpi(
            edu.wpi.first.units.measure.AngularAcceleration angularAcceleration) {
        return AngularAcceleration.radiansPerSecondSquared(
                angularAcceleration.in(RadiansPerSecondPerSecond));
    }

    // Mass
    /**
     * Converts from our {@link Mass} unit into WPI's {@link edu.wpi.first.units.measure.Mass} unit.
     *
     * @param mass our {@link Mass} unit
     * @return WPI's {@link edu.wpi.first.units.measure.Mass}
     */
    public static edu.wpi.first.units.measure.Mass toWpi(Mass mass) {
        return Kilograms.of(mass.asKilograms());
    }

    /**
     * Converts from WPI's {@link edu.wpi.first.units.measure.Mass} unit into our equivalent
     * {@link Mass} unit.
     *
     * @param mass WPI's {@link edu.wpi.first.units.measure.Mass}
     * @return {@link Mass} unit
     */
    public static Mass fromWpi(edu.wpi.first.units.measure.Mass mass) {
        return Mass.kilograms(mass.in(Kilograms));
    }

    // Moment of Inertia
    /**
     * Converts from our {@link MomentOfInertia} unit into WPI's
     * {@link edu.wpi.first.units.measure.MomentOfInertia} unit.
     *
     * @param momentOfInertia our {@link MomentOfInertia} unit
     * @return WPI's {@link edu.wpi.first.units.measure.MomentOfInertia}
     */
    public static edu.wpi.first.units.measure.MomentOfInertia toWpi(MomentOfInertia momentOfInertia) {
        return KilogramSquareMeters.of(momentOfInertia.asKilogramMetersSquared());
    }

    /**
     * Converts from WPI's {@link edu.wpi.first.units.measure.MomentOfInertia} unit into our
     * equivalent {@link MomentOfInertia} unit.
     *
     * @param momentOfInertia WPI's {@link edu.wpi.first.units.measure.MomentOfInertia}
     * @return our {@link MomentOfInertia} unit
     */
    public static MomentOfInertia fromWpi(
            edu.wpi.first.units.measure.MomentOfInertia momentOfInertia) {
        return MomentOfInertia.kilogramMetersSquared(momentOfInertia.in(KilogramSquareMeters));
    }
}
