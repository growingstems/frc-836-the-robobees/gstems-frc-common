/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.frc.util;

import edu.wpi.first.wpilibj.DriverStation;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class is used to identify which RoboRIO the code is currently being ran on. It accepts
 * anything as the value that gets stored in the internal map, but its recommended to use an enum
 * representing your robots.
 *
 * @param <T> the class used to represent all the robots that are known and what their MAC
 *     address(es) are
 */
public class RobotIdentifier<T extends RobotIdentification> {
    private final Map<String, T> m_macAddresses = new HashMap<>();
    private final T m_defaultRobot;

    /**
     * Creates a {@link RobotIdentification} used to ID the robot that will use all possible enum
     * values. If the MAC address isn't found when {@link determineRobot} is called, the given
     * {@code defaultRobot} is used instead.
     *
     * @param defaultRobot the default robot value that is used if the MAC address doesn't match what
     *     is provided.
     * @return a {@link RobotIdentification} using all enum options
     * @param <E> The enum-based {@link RobotIdentification}
     */
    public static <E extends Enum<E> & RobotIdentification> RobotIdentifier<E> fromEnum(
            E defaultRobot) {
        return new RobotIdentifier<E>(defaultRobot, Arrays.asList(getEnumConstants(defaultRobot)));
    }

    /**
     * Creates a RobotIdentifier used to ID the robot. If the MAC address isn't found when
     * {@link determineRobot} is called, the given {@code defaultRobot} is used instead.
     *
     * @param defaultRobot the default robot value that is used if the MAC address doesn't match what
     *     is provided.
     * @param options the list of all possible robot options to search for.
     */
    public RobotIdentifier(T defaultRobot, List<T> options) {
        m_defaultRobot = defaultRobot;

        for (T option : options) {
            for (var macAddress : option.getMacAddresses()) {
                m_macAddresses.put(macAddress.toUpperCase(), option);
            }
        }
    }

    /**
     * Checks all the network interfaces on the device the code is running on, and checks if there are
     * any matches. If there are no matches, the default robot is used. If there are multiple matches,
     * the first one found is used.
     *
     * @return the robot that first matches one of the MAC addresses, or the default robot if none is
     *     found
     */
    public T determineRobot() {
        T identifier = null;

        Enumeration<NetworkInterface> networkInterfaces;
        try {
            networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                NetworkInterface ni = networkInterfaces.nextElement();
                byte[] hardwareAddress = ni.getHardwareAddress();
                if (hardwareAddress != null) {
                    String[] hexadecimalFormat = new String[hardwareAddress.length];
                    for (int i = 0; i < hardwareAddress.length; i++) {
                        hexadecimalFormat[i] = String.format("%02X", hardwareAddress[i]);
                    }

                    var address = String.join("-", hexadecimalFormat).toUpperCase();
                    System.out.println("Found MAC Address: " + address);

                    var mapIdentifier = m_macAddresses.get(address);
                    if (mapIdentifier != null) {
                        if (identifier == null) {
                            identifier = mapIdentifier;
                        } else if (!identifier.equals(mapIdentifier)) {
                            DriverStation.reportWarning(
                                    "Found multiple robots' MAC Addresses. Using first address seen.", false);
                        }
                    }
                }
            }
        } catch (SocketException e) {
            DriverStation.reportError("Error determining MAC Addresses", true);
        }

        if (identifier == null) {
            DriverStation.reportError("No Robot MAC Address found! Using default.", false);
            identifier = m_defaultRobot;
        }

        System.out.println("Robot Determined to be: " + identifier.toString());
        return identifier;
    }

    private static <T extends Enum<T>> T[] getEnumConstants(T sample) {
        @SuppressWarnings("unchecked")
        var c = (Class<T>) sample.getClass();
        return c.getEnumConstants();
    }
}
