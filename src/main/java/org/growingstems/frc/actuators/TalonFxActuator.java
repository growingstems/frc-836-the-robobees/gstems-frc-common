/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.frc.actuators;

import com.ctre.phoenix6.StatusSignal;
import com.ctre.phoenix6.controls.DynamicMotionMagicVoltage;
import com.ctre.phoenix6.controls.MotionMagicVelocityVoltage;
import com.ctre.phoenix6.controls.MotionMagicVoltage;
import com.ctre.phoenix6.controls.NeutralOut;
import com.ctre.phoenix6.controls.VoltageOut;
import com.ctre.phoenix6.hardware.TalonFX;
import org.growingstems.control.actuators.MotorActuator;
import org.growingstems.control.actuators.PositionActuator;
import org.growingstems.control.actuators.VelocityActuator;
import org.growingstems.frc.util.Convert;
import org.growingstems.math.RangeU;
import org.growingstems.measurements.Measurements.Current;
import org.growingstems.measurements.Measurements.DivByTime;
import org.growingstems.measurements.Measurements.DivByTimeSquared;
import org.growingstems.measurements.Measurements.Length;
import org.growingstems.measurements.Measurements.MulByTime;
import org.growingstems.measurements.Measurements.MulByTimeSquared;
import org.growingstems.measurements.Measurements.Temperature;
import org.growingstems.measurements.Measurements.Time;
import org.growingstems.measurements.Measurements.Velocity;
import org.growingstems.measurements.Measurements.Voltage;
import org.growingstems.measurements.Unit;

/**
 * Represents an actuator, based on a Talon FX motor controller, that can both do open loop control
 * and also closed loop control using a feedback sensor capable of measuring the actuator's current
 * position and velocity. References to the feedback sensor's native units are referred to as as a
 * sensor unit (SU). Also supports acceleration to a limited capacity.
 *
 * <p>When using Position control modes, this actuator utilizes Motion Magic on slot 0, meaning
 * Motion Magic and slot 0 should be configured externally to this class.
 *
 * <p>When using Velocity control modes, this actuator utilizes Velocity Motion Magic on slot 0,
 * meaning Motion Magic and slot 0 should be configured externally to this class.
 *
 * @param <P> the position unit used by this actuator
 * @param <V> the velocity unit used by this actuator
 * @param <A> the acceleration unit used by this actuator
 */
public class TalonFxActuator<
                P extends Unit<P> & DivByTime<V> & DivByTimeSquared<A>,
                V extends Unit<V> & MulByTime<P> & DivByTime<A>,
                A extends Unit<A> & MulByTime<V> & MulByTimeSquared<P>>
        extends VelocityActuator<P, V> implements CtreActuator<P, V, A> {
    /** Number of acceleration units per single sensor unit of the actuator's motor controller. */
    protected final A m_accelerationUnitPerSensorUnit;
    /** Represents a zero in the acceleration units being used. */
    protected final A m_zeroA;

    /** The TalonFX motor to being used as the controller for the actuator */
    protected final TalonFX m_motor;

    // Phoenix 6 Signals
    private final StatusSignal<edu.wpi.first.units.measure.Angle> m_getPosition;
    private final StatusSignal<edu.wpi.first.units.measure.AngularVelocity> m_getVelocity;
    private final StatusSignal<edu.wpi.first.units.measure.Temperature> m_getTemperature;
    private final StatusSignal<edu.wpi.first.units.measure.Voltage> m_getOutputVoltage;
    private final StatusSignal<edu.wpi.first.units.measure.Voltage> m_getSupplyVoltage;
    private final StatusSignal<edu.wpi.first.units.measure.Current> m_getTorqueCurrent;
    private final StatusSignal<edu.wpi.first.units.measure.Current> m_getSupplyCurrent;

    /**
     * Creates a TalonFX velocity actuator with the given parameters.
     *
     * @param motor the TalonFX motor to be used as the controller for the actuator
     * @param unitPerSensorUnit the conversion factor in {@code P} units for every native sensor unit
     *     of the actuator's feedback sensor
     * @param startingPositionOffset the offset applied to the actuator's position after being
     *     converted to {@code P} units
     * @param positionRange the max allowed positional range of the actuator in {@code P} units after
     *     the conversion factor and offset is applied
     * @param velocityRange the max allowed velocity range of the actuator in {@code V} units after
     *     the conversion factor is applied
     */
    public TalonFxActuator(
            TalonFX motor,
            P unitPerSensorUnit,
            P startingPositionOffset,
            RangeU<P> positionRange,
            RangeU<V> velocityRange) {
        super(unitPerSensorUnit, startingPositionOffset, positionRange, velocityRange);

        m_motor = motor;

        m_accelerationUnitPerSensorUnit = m_velocityUnitPerSensorUnit.div(Time.seconds(1.0));
        m_zeroA = m_accelerationUnitPerSensorUnit.sub(m_accelerationUnitPerSensorUnit);

        // Phoenix 6 Signals
        m_getPosition = m_motor.getPosition();
        m_getVelocity = m_motor.getVelocity();
        m_getTemperature = m_motor.getDeviceTemp();
        m_getOutputVoltage = m_motor.getMotorVoltage();
        m_getSupplyVoltage = m_motor.getSupplyVoltage();
        m_getTorqueCurrent = m_motor.getTorqueCurrent();
        m_getSupplyCurrent = m_motor.getSupplyCurrent();
    }

    /**
     * Creates a TalonFX position actuator with the given parameters.
     *
     * @param <P> the position unit used by this actuator
     * @param <V> the velocity unit used by this actuator
     * @param <A> the acceleration unit used by this actuator
     * @param motor the TalonFX motor to be used as the controller for the actuator
     * @param unitPerSensorUnit the conversion factor in {@code P} units for every native sensor unit
     *     of the actuator's feedback sensor
     * @param startingPositionOffset the offset applied to the actuator's position after being
     *     converted to {@code P} units
     * @param positionRange the max allowed positional range of the actuator in {@code P} units after
     *     the conversion factor and offset is applied
     * @return a PositionActuator
     */
    public static <
                    P extends Unit<P> & DivByTime<V> & DivByTimeSquared<A>,
                    V extends Unit<V> & MulByTime<P> & DivByTime<A>,
                    A extends Unit<A> & MulByTime<V> & MulByTimeSquared<P>>
            PositionActuator<P> positionActuator(
                    TalonFX motor, P unitPerSensorUnit, P startingPositionOffset, RangeU<P> positionRange) {
        var vNonZeroValue = unitPerSensorUnit.div(Time.seconds(1.0));
        var vZero = vNonZeroValue.sub(vNonZeroValue);

        var velocityRange = new RangeU<>(vZero, vZero);

        return new TalonFxActuator<>(
                motor, unitPerSensorUnit, startingPositionOffset, positionRange, velocityRange);
    }

    /**
     * Creates a TalonFX motor actuator.
     *
     * @param motor the TalonFX motor to be used as the controller for the actuator
     * @return a MotorActuator
     */
    public static MotorActuator motorActuator(TalonFX motor) {
        var pZero = Length.ZERO;
        var positionRange = new RangeU<>(pZero, pZero);
        var velocityRange = new RangeU<>(Velocity.ZERO, Velocity.ZERO);

        return new TalonFxActuator<>(motor, pZero, pZero, positionRange, velocityRange);
    }

    /**
     * Returns the TalonFX motor controller that is being used by this actuator.
     *
     * @return the TalonFX motor controller that is being used
     */
    public TalonFX getMotor() {
        return m_motor;
    }

    @Override
    public void stop() {
        m_motor.setControl(new NeutralOut());
    }

    @Override
    protected void setHalVelocity(double velocity_sups) {
        setHalVelocity(velocity_sups, Voltage.ZERO);
    }

    @Override
    protected void setHalVelocity(double velocity_sups, Voltage arbitraryFeedForward) {
        setHalVelocity(velocity_sups, 0.0, Voltage.ZERO);
    }

    /**
     * Uses CTRE's Motion Magic Velocity control mode to control the actuator's motor controller given
     * a velocity to go to, and an acceleration to follow.
     *
     * @param velocity_sups the velocity to follow in the actuator's native velocity sensor units
     * @param accel_supsps the acceleration to follow in the actuator's native acceleration sensor
     *     units
     * @param arbitraryFeedForward the amount of additional voltage to be applied to the actuator's
     *     motor controller
     */
    protected void setHalVelocity(
            double velocity_sups, double accel_supsps, Voltage arbitraryFeedForward) {
        m_motor.setControl(new MotionMagicVelocityVoltage(velocity_sups)
                .withAcceleration(accel_supsps)
                .withFeedForward(Convert.toWpi(arbitraryFeedForward)));
    }

    @Override
    protected double getHalVelocity_sups() {
        return m_getVelocity.refresh().getValueAsDouble();
    }

    @Override
    protected void setHalPosition(double position_su) {
        setHalPosition(position_su, Voltage.ZERO);
    }

    @Override
    protected void setHalPosition(double position_su, Voltage arbitraryFeedForward) {
        m_motor.setControl(
                new MotionMagicVoltage(position_su).withFeedForward(Convert.toWpi(arbitraryFeedForward)));
    }

    /**
     * Uses CTRE's Dynamic Motion Magic control mode to control the actuator's motor controller given
     * a position to go to, and a velocity, acceleration and jerk to follow. Dynamic Motion Magic
     * currently is a pro feature and the motor controller needs to have an active pro licenses to
     * work with this function.
     *
     * @param position_su the position to go to in the actuator's native sensor units
     * @param velocity_sups the cruise velocity to follow in the actuator's native velocity sensor
     *     units
     * @param accel_supsps the acceleration to follow in the actuator's native acceleration sensor
     *     units
     * @param jerk_supspsps the jerk to follow in the actuator's native jerk sensor units
     * @param arbitraryFeedForward the amount of additional voltage to be applied to the actuator's
     *     motor controller
     */
    protected void setHalPosition(
            double position_su,
            double velocity_sups,
            double accel_supsps,
            double jerk_supspsps,
            Voltage arbitraryFeedForward) {
        m_motor.setControl(
                new DynamicMotionMagicVoltage(position_su, velocity_sups, accel_supsps, jerk_supspsps)
                        .withFeedForward(Convert.toWpi(arbitraryFeedForward)));
    }

    @Override
    protected double getHalPosition_su() {
        return m_getPosition.refresh().getValueAsDouble();
    }

    @Override
    protected void setHalVoltage(Voltage power) {
        m_motor.setControl(new VoltageOut(Convert.toWpi(power)));
    }

    @Override
    public Temperature getTemperature() {
        return Convert.fromWpi(m_getTemperature.refresh().getValue());
    }

    @Override
    public Voltage getSupplyVoltage() {
        return Convert.fromWpi(m_getSupplyVoltage.refresh().getValue());
    }

    @Override
    public Voltage getOutputVoltage() {
        return Convert.fromWpi(m_getOutputVoltage.refresh().getValue());
    }

    @Override
    public Current getStatorCurrent() {
        return Convert.fromWpi(m_getTorqueCurrent.refresh().getValue());
    }

    @Override
    public Current getSupplyCurrent() {
        return Convert.fromWpi(m_getSupplyCurrent.refresh().getValue());
    }

    @Override
    public void setPosition(P position, Voltage arbitraryFeedForward) {
        if (m_disabled) {
            stop();
            return;
        }

        m_goalPosition = m_positionalRange.coerceValue(position);
        setHalPosition(positionToSensorUnits(m_goalPosition), arbitraryFeedForward);
    }

    /**
     * Converts a value in acceleration units into the actuator motor controller's native acceleration
     * sensor units.
     *
     * @param acceleration the acceleration value to convert
     * @return the converted value in the actuator motor controller's native acceleration sensor units
     */
    protected double accelerationToSensorUnits(A acceleration) {
        return acceleration.div(m_accelerationUnitPerSensorUnit).asNone();
    }

    @Override
    public void setVelocity(V velocity, A acceleration, Voltage arbitraryFeedForward) {
        if (m_disabled) {
            stop();
            return;
        }

        m_goalVelocity = m_velocityRange.coerceValue(velocity);
        setHalVelocity(
                velocityToSensorUnits(velocity),
                accelerationToSensorUnits(acceleration),
                arbitraryFeedForward);
    }
}
