/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.frc.actuators;

import org.growingstems.control.actuators.DummyActuator;
import org.growingstems.math.RangeU;
import org.growingstems.measurements.Measurements.Current;
import org.growingstems.measurements.Measurements.DivByTime;
import org.growingstems.measurements.Measurements.DivByTimeSquared;
import org.growingstems.measurements.Measurements.MulByTime;
import org.growingstems.measurements.Measurements.MulByTimeSquared;
import org.growingstems.measurements.Measurements.Temperature;
import org.growingstems.measurements.Measurements.Voltage;
import org.growingstems.measurements.Unit;

/**
 * A dummy actuator class meant to fake a TalonFX actuator that user code expects to exist, but the
 * hardware might not be present. The only supported feature is that this actuator will always say
 * its at the position or velocity that you tell it to go to.
 */
public class DummyCtreActuator<
                P extends Unit<P> & DivByTime<V> & DivByTimeSquared<A>,
                V extends Unit<V> & MulByTime<P> & DivByTime<A>,
                A extends Unit<A> & MulByTime<V> & MulByTimeSquared<P>>
        extends DummyActuator<P, V> implements CtreActuator<P, V, A> {
    /**
     * Creates a CTRE dummy velocity actuator which can also function as a position and motor
     * actuator.
     *
     * @param posNonZeroValue a non-zero value for the {@code P} unit
     */
    public DummyCtreActuator(P posNonZeroValue) {
        super(posNonZeroValue);
    }

    /**
     * Creates a CTRE dummy velocity actuator with the given parameters, which can also function as a
     * position and motor actuator.
     *
     * @param unitPerSensorUnit the conversion factor in {@code P} units for every native sensor unit
     *     of the actuator's feedback sensor
     * @param startingPositionOffset the offset applied to the actuator's position after being
     *     converted to {@code P} units
     * @param positionRange the max positional range of the actuator in {@code P} units after the
     *     conversion factor and offset is applied
     * @param velocityRange the max velocity range of the actuator in {@code V} units after the
     *     conversion factor is applied
     */
    public DummyCtreActuator(
            P unitPerSensorUnit,
            P startingPositionOffset,
            RangeU<P> positionRange,
            RangeU<V> velocityRange) {
        super(unitPerSensorUnit, startingPositionOffset, positionRange, velocityRange);
    }

    @Override
    public Temperature getTemperature() {
        return Temperature.ZERO;
    }

    @Override
    public Voltage getSupplyVoltage() {
        return Voltage.ZERO;
    }

    @Override
    public Voltage getOutputVoltage() {
        return Voltage.ZERO;
    }

    @Override
    public Current getStatorCurrent() {
        return Current.ZERO;
    }

    @Override
    public Current getSupplyCurrent() {
        return Current.ZERO;
    }

    @Override
    public void setVelocity(V velocity, A acceleration, Voltage arbitraryFeedForward) {
        setVelocity(velocity);
    }
}
