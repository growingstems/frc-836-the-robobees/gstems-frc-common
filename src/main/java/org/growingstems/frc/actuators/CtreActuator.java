/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

package org.growingstems.frc.actuators;

import org.growingstems.measurements.Measurements.Current;
import org.growingstems.measurements.Measurements.DivByTime;
import org.growingstems.measurements.Measurements.DivByTimeSquared;
import org.growingstems.measurements.Measurements.MulByTime;
import org.growingstems.measurements.Measurements.MulByTimeSquared;
import org.growingstems.measurements.Measurements.Temperature;
import org.growingstems.measurements.Measurements.Voltage;
import org.growingstems.measurements.Unit;

/** Interface for actuators that are based on a CTRE motor controller(s). */
public interface CtreActuator<
        P extends Unit<P> & DivByTime<V> & DivByTimeSquared<A>,
        V extends Unit<V> & MulByTime<P> & DivByTime<A>,
        A extends Unit<A> & MulByTime<V> & MulByTimeSquared<P>> {

    /**
     * Gets the temperature measured by the actuator motor.
     *
     * @return the measured temperature
     */
    Temperature getTemperature();

    /**
     * Gets the supply side voltage to the actuator motor.
     *
     * @return the supply side voltage
     */
    Voltage getSupplyVoltage();

    /**
     * Gets the output voltage being applied to the output of the actuator motor.
     *
     * @return the output voltage to the actuator
     */
    Voltage getOutputVoltage();

    /**
     * Gets the output stator current being applied to the actuator motor's stator.
     *
     * @return the output stator current
     */
    Current getStatorCurrent();

    /**
     * Gets the supply side current draw of the actuator motor.
     *
     * @return the supply side current draw
     */
    Current getSupplyCurrent();

    /**
     * Commands the actuator to run in closed loop velocity mode. The actuator will actively try to go
     * to the requested velocity, with the given acceleration, based on the velocity PID settings of
     * the actuator's controller.
     *
     * @param velocity the velocity that the actuator is being commanded to
     * @param acceleration the acceleration the actuator will use when changing velocity
     */
    default void setVelocity(V velocity, A acceleration) {
        setVelocity(velocity, acceleration, Voltage.ZERO);
    }

    /**
     * Commands the actuator to run in closed loop velocity mode with an additional feed forward term.
     * The actuator will actively try to go to the requested velocity, with the given acceleration,
     * based on the velocity PID settings of the actuator's controller. Use the feed forward term to
     * add an additional voltage offset to the closed loop controller.
     *
     * @param velocity the velocity that the actuator is being commanded to
     * @param acceleration the acceleration the actuator will use when changing velocity
     * @param arbitraryFeedForward the amount of additional voltage to be applied to the actuator
     */
    void setVelocity(V velocity, A acceleration, Voltage arbitraryFeedForward);
}
